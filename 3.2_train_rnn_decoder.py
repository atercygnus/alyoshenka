import math

import numpy as np

import torch
from torch import nn
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms, datasets

from tqdm import tqdm
import pickle
import shutil

from rnn_decoder import RNNModel
from cnn_encoder import CNNEncoder

from os.path import join
from torchvision.models import inception_v3

from utils import PAD, batch_captions_to_matrix, caption_tokens_to_indices

print('Loading data needed for training...')

f = open('tcapts_matrix.pkl', 'rb')
tcapts_matrix = pickle.load(f)

f = open('vcapts_matrix.pkl', 'rb')
vcapts_matrix = pickle.load(f)

f = open('train_features.pkl', 'rb')
tfeats = pickle.load(f)

# f = open('vfeats.pkl', 'rb')
# vfeats = pickle.load(f)

f = open('vocabulary.pkl', 'rb')
vocab = pickle.load(f)

f = open('vocabulary_inverse.pkl', 'rb')
vocab_inverse = pickle.load(f)

print('Data loaded!')

tcapts_matrix = torch.Tensor(tcapts_matrix).long()
tfeats = torch.Tensor(np.expand_dims(tfeats, 0))

PAD = "#PAD#"
UNK = "#UNK#"
START = "#START#"
END = "#END#"

images_count = tcapts_matrix.shape[0]
BATCH_SIZE = 32
BATCH_COUNT = int(np.ceil(images_count / BATCH_SIZE))
datadir = 'C:\\associative_representations_data\\'

preprocess = transforms.Compose([
    transforms.Resize(299),
    transforms.CenterCrop(299),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

train = datasets.CocoCaptions(
    join(datadir, 'train2017'),
    annFile=join(datadir, 'captions_train2017.json'),
    transform=preprocess)

trainset = torch.utils.data.DataLoader(train, batch_size=BATCH_SIZE)

TENSORBOARD_DIR = 'E:\\alyoshenka\\runs\\run_5'
SAVE_MODEL_PATH = '.\\models\\decoder'
shutil.rmtree(TENSORBOARD_DIR)

#encoder = inception_v3(pretrained=True).cuda()
#encoder.fc = nn.Identity()
#encoder.eval()

encoder = CNNEncoder().cuda()

decoder = RNNModel('GRU', ntoken=len(vocab), ninp=RNNModel.WORD_EMBED_SIZE, nhid=RNNModel.LSTM_UNITS, nlayers=1, dropout=0).cuda()
decoder.load_state_dict(torch.load('models\decoder_final'))
criterion = nn.CrossEntropyLoss(reduction='none')
optimizer = torch.optim.Adam(list(decoder.parameters())+list(encoder.parameters()), lr=1e-4)

writer = SummaryWriter(log_dir=TENSORBOARD_DIR, filename_suffix='AdamW')

EVERY_BATCHES_TOSHOW = 100

EPOCHS = 140
total_loss = 0
perplexity = []
best_ppl = np.inf
n_iter = 0


def prepare_batch(captions):

    indices = caption_tokens_to_indices(np.array(captions).T, vocab, vocab[PAD]).reshape(-1)
    matrix = batch_captions_to_matrix(indices, vocab[PAD], max_len=52).reshape(-1, 5, 52)

    return torch.Tensor(matrix).long()


for e in tqdm(range(EPOCHS)):

    total_loss = 0

    for i, (images, captions) in enumerate(trainset):

        decoder.train()
        encoder.train()

        train_caption_ix = np.random.randint(0, 4)
        test_caption_ix = 4

        captions_matrix = prepare_batch(captions)

        #with torch.no_grad():
        image_embeddings = encoder(images.cuda()).unsqueeze(dim=0)

        train_captions = captions_matrix[:, train_caption_ix, :]
        train_capts = train_captions[:, :-1].cuda()
        train_target = train_captions[:, 1:]

        decoder.zero_grad()

        out, _ = decoder(train_capts, image_embeddings.cuda())

        loss = criterion(out, train_target.reshape(-1).cuda())
        mask = torch.Tensor(list(map(lambda x: int(x != vocab[PAD]), train_target.reshape(-1)))).cuda()

        loss = (loss * mask).mean()

        loss.backward()
        optimizer.step()

        decoder.eval()
        encoder.eval()

        test_batch = tcapts_matrix[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, test_caption_ix, :]
        image_embeddings = tfeats[:, i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :]

        test_capts = test_batch[:, :-1].cuda()
        test_target = test_batch[:, 1:]

        out, _ = decoder(test_capts, image_embeddings.cuda())
        test_loss = criterion(out, test_target.reshape(-1).cuda())
        test_mask = torch.Tensor(list(map(lambda x: int(x != vocab[PAD]), test_target.reshape(-1)))).cuda()
        test_loss = (test_loss * test_mask).mean()

        total_loss += test_loss.item()

        if i > 0 and i % EVERY_BATCHES_TOSHOW == 0:
            ppl = math.exp(total_loss / EVERY_BATCHES_TOSHOW)

            loss = loss.cpu().detach().numpy()
            perplexity.append(ppl)
            total_loss = 0

            writer.add_scalar('Loss', loss, n_iter)
            writer.add_scalar('ppl', ppl, n_iter)
            n_iter += 1

writer.close()
