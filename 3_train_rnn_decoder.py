import math
import numpy as np

import torch
from torch import nn
from torch.utils.tensorboard import SummaryWriter

from tqdm import tqdm
import pickle
import shutil

from rnn_decoder import RNNModel

print('Loading data needed for training...')

f = open('tcapts_matrix.pkl', 'rb')
tcapts_matrix = pickle.load(f)

f = open('vcapts_matrix.pkl', 'rb')
vcapts_matrix = pickle.load(f)

f = open('train_features.pkl', 'rb')
tfeats = pickle.load(f)

# f = open('vfeats.pkl', 'rb')
# vfeats = pickle.load(f)

f = open('vocabulary.pkl', 'rb')
vocab = pickle.load(f)

f = open('vocabulary_inverse.pkl', 'rb')
vocab_inverse = pickle.load(f)

print('Data loaded!')

tcapts_matrix = torch.Tensor(tcapts_matrix).long()
tfeats = torch.Tensor(np.expand_dims(tfeats, 0))

PAD = "#PAD#"
UNK = "#UNK#"
START = "#START#"
END = "#END#"

TENSORBOARD_DIR = 'E:\\alyoshenka\\runs'
SAVE_MODEL_PATH = 'decoder'
shutil.rmtree(TENSORBOARD_DIR)

decoder = RNNModel('GRU', ntoken=len(vocab), ninp=RNNModel.WORD_EMBED_SIZE, nhid=RNNModel.LSTM_UNITS, nlayers=1, dropout=0).cuda()
criterion = nn.CrossEntropyLoss(reduction='none')
optimizer = torch.optim.Adam(decoder.parameters(), lr=1e-4)

writer = SummaryWriter(log_dir=TENSORBOARD_DIR)

images_count = tcapts_matrix.shape[0]
BATCH_SIZE = 32
BATCH_COUNT = int(np.ceil(images_count / BATCH_SIZE))

EVERY_BATCHES_TOSHOW = 100

EPOCHS = 1
total_loss = 0
perplexity = []
n_iter = 0

print('Start training model in {} epochs'.format(EPOCHS))

for e in tqdm(range(EPOCHS)):

    total_loss = 0

    for i in range(BATCH_COUNT):

        decoder.train()

        train_caption_ix = np.random.randint(0, 4)
        test_caption_ix = 4

        train_batch = tcapts_matrix[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, train_caption_ix, :]
        image_embedings = tfeats[:, i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :]

        train_capts = train_batch[:, :-1].cuda()
        train_target = train_batch[:, 1:]

        decoder.zero_grad()

        out, _ = decoder(train_capts, image_embedings.cuda())

        loss = criterion(out, train_target.reshape(-1).cuda())
        mask = torch.Tensor(list(map(lambda x: int(x != vocab[PAD]), train_target.reshape(-1)))).cuda()

        loss = (loss * mask).mean()

        loss.backward()
        optimizer.step()

        decoder.eval()

        test_batch = tcapts_matrix[i * BATCH_SIZE:(i + 1) * BATCH_SIZE, test_caption_ix, :]
        image_embedings = tfeats[:, i * BATCH_SIZE:(i + 1) * BATCH_SIZE, :]

        test_capts = test_batch[:, :-1].cuda()
        test_target = test_batch[:, 1:]

        out, _ = decoder(test_capts, image_embedings.cuda())
        test_loss = criterion(out, test_target.reshape(-1).cuda())
        test_mask = torch.Tensor(list(map(lambda x: int(x != vocab[PAD]), test_target.reshape(-1)))).cuda()
        test_loss = (test_loss * test_mask).mean()

        total_loss += test_loss.item()

        if i > 0 and i % EVERY_BATCHES_TOSHOW == 0:
            ppl = math.exp(total_loss / EVERY_BATCHES_TOSHOW)
            loss = loss.cpu().detach().numpy()
            perplexity.append(ppl)
            total_loss = 0

            writer.add_scalar('Loss', loss, n_iter)
            writer.add_scalar('ppl', ppl, n_iter)
            n_iter += 1

print('Training complete!\n')

print('Saving trained model in {} file'.format(SAVE_MODEL_PATH))

torch.save(decoder.state_dict(), SAVE_MODEL_PATH)

