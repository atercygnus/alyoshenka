from os.path import join

import pickle

import torch
import torch.nn as nn
from torchvision import transforms, datasets
from torchvision.models import inception_v3

import numpy as np
from tqdm import tqdm

IMG_SIZE = (299, 299)
data_directory = 'C:\\associative_representations_data\\'

preprocessing = transforms.Compose([
    transforms.Resize(299),
    transforms.CenterCrop(299),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
])

model = inception_v3(pretrained=True)
model.fc = nn.Identity()
model.eval()
model.to('cuda')

print('Embedding train images...')

train = datasets.CocoCaptions(
    join(data_directory, 'train2017'),
    annFile=join(data_directory, 'captions_train2017.json'),
    transform=preprocessing)

train_set = torch.utils.data.DataLoader(train, batch_size=32, shuffle=True)

train_embeds = torch.tensor([])
train_captions = None

for images, captions in tqdm(train_set):

    with torch.no_grad():
        embedded_batch = model(images.cuda())
    train_embeds = torch.cat((train_embeds, embedded_batch.cpu()), 0)

    captions = np.array(captions).T
    if train_captions is None:
        train_captions = captions
    else:
        train_captions = np.vstack((train_captions, captions))

with open('train_features.pkl', 'wb') as file_embeds:
    pickle.dump(train_embeds.numpy(), file_embeds)

with open('train_captions.pkl', 'wb') as file_captions:
    pickle.dump(train_captions, file_captions)

print('Done!\n')
print('Embedding train images...')

val = datasets.CocoCaptions(
    join(data_directory, 'val2017'),
    annFile=join(data_directory, 'captions_val2017.json'),
    transform=preprocessing)

val_set = torch.utils.data.DataLoader(val, batch_size=32, shuffle=True)

validation_embeds = torch.tensor([])
validation_captions = None

for images, captions in tqdm(val_set):

    with torch.no_grad():
        embedded_batch = model(images.cuda())
        validation_embeds = torch.cat((validation_embeds, embedded_batch.cpu()), 0)

    captions = np.array(captions).T
    if validation_captions is None:
        validation_captions = captions
    else:
        validation_captions = np.vstack((validation_captions, captions))

with open('validation_features.pkl', 'wb') as file_embeds:
    pickle.dump(validation_embeds.numpy(), file_embeds)

with open('validation_captions.pkl', 'wb') as file_captions:
    pickle.dump(validation_captions, file_captions)

print('Done!\n')
