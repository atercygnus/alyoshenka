import torch
from torch import nn


class RNNModel(nn.Module):
    IMG_EMBED_SIZE = 2048
    IMG_EMBED_BOTTLENECK = 120
    WORD_EMBED_SIZE = 100
    LSTM_UNITS = 300
    LOGIT_BOTTLENECK = 120
    pad_idx = 0

    def __init__(self, rnn_type, ntoken, ninp, nhid, nlayers, dropout=0.5):

        super(RNNModel, self).__init__()

        self.drop = nn.Dropout(dropout)

        self.img_embed_to_bottleneck = nn.Linear(self.IMG_EMBED_SIZE, self.IMG_EMBED_BOTTLENECK)
        self.img_embed_bottleneck_to_h0 = nn.Linear(self.IMG_EMBED_BOTTLENECK, nhid)

        self.embedding = nn.Embedding(ntoken, ninp)

        if rnn_type == 'LSTM':
            self.rnn = nn.LSTM(ninp, nhid, nlayers, dropout=dropout, batch_first=True)
        elif rnn_type == 'GRU':
            self.rnn = nn.GRU(ninp, nhid, nlayers, dropout=dropout, batch_first=True)

        self.token_logits_bottleneck = nn.Linear(nhid, self.LOGIT_BOTTLENECK)
        self.token_logits = nn.Linear(self.LOGIT_BOTTLENECK, ntoken)

        self.init_weights()

        self.rnn_type = rnn_type
        self.nhid = nhid
        self.nlayers = nlayers

    def init_weights(self):

        initrange = 0.1
        self.embedding.weight.data.uniform_(-initrange, initrange)

        self.img_embed_to_bottleneck.bias.data.fill_(0)
        self.img_embed_to_bottleneck.weight.data.uniform_(-initrange, initrange)

        self.img_embed_bottleneck_to_h0.bias.data.fill_(0)
        self.img_embed_bottleneck_to_h0.weight.data.uniform_(-initrange, initrange)

        self.token_logits_bottleneck.bias.data.fill_(0)
        self.token_logits_bottleneck.weight.data.uniform_(-initrange, initrange)

        self.token_logits.bias.data.fill_(0)
        self.token_logits.weight.data.uniform_(-initrange, initrange)

    def forward(self, x, hidden=None):

        if hidden.shape[2] == self.IMG_EMBED_SIZE:
            hidden = self.img_embed_bottleneck_to_h0(self.img_embed_to_bottleneck(hidden))
            hidden = torch.cat([hidden] * self.nlayers)

        word_embeds = self.drop(self.embedding(x))
        output, hidden = self.rnn(word_embeds, hidden)
        output = self.drop(output)

        if not output.is_contiguous():
            output = output.contiguous()
        output_bottleneck = self.token_logits_bottleneck(
            output.view(output.shape[0] * output.shape[1], output.shape[2]))
        output = self.token_logits(output_bottleneck)

        return output, hidden
