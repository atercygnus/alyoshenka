import numpy as np

import pickle
import re

from collections import Counter

f = open('train_captions.pkl', 'rb')
train_captions = pickle.load(f)

f = open('validation_captions.pkl', 'rb')
validation_captions = pickle.load(f)

PAD = "#PAD#"
UNK = "#UNK#"
START = "#START#"
END = "#END#"


def split_sentence(sentence):
    return list(filter(lambda x: len(x) > 0, re.split('\W+', sentence.lower())))


def generate_vocabulary(captions):

    w_bag = [PAD, START, UNK, END] + \
            [item.lower() for sublist in [split_sentence(sent) for sent in captions] for item in sublist]

    vocab = Counter(w_bag)
    vocab = [PAD, START, UNK, END] + [token for token, cnt in zip(vocab.keys(), vocab.values()) if cnt >= 5]
    vocab = {key: i for i, key in enumerate(vocab)}

    return vocab


vocabulary = generate_vocabulary(train_captions.reshape(-1))
vocabulary_inverse = {idx: w for w, idx in vocabulary.items()}

print('Vocabulary now contains {} words'.format(len(vocabulary)))

with open('vocabulary.pkl', 'wb') as f:
    pickle.dump(vocabulary, f)

with open('vocabulary_inverse.pkl', 'wb') as f:
    pickle.dump(vocabulary_inverse, f)


def internal_caption_tokens_to_indices(captions, vocab, pad_token):
    return [
        [vocab[START]] +
        [vocab[word] if word in vocab else vocab[UNK]
         for word
         in split_sentence(capt)] + [vocab[END]] for capt in captions]


def caption_tokens_to_indices(captions, vocab, pad_token):
    return np.array([
        internal_caption_tokens_to_indices(captions_batch, vocab, pad_token)
        for captions_batch
        in captions.T]).T


def batch_captions_to_matrix(batch_captions, pad_idx, max_len=None):
    if max_len is None:
        pad_len = max(map(len, batch_captions))
    else:
        pad_len = max(max(map(len, batch_captions)), max_len)

    matrix = []
    for capt in batch_captions:
        if pad_len - len(capt) >= 0:
            matrix.append(np.pad(capt, (0, pad_len - len(capt)), mode='constant', constant_values=pad_idx))
        else:
            matrix.append(capt[:pad_len])

    return np.array(matrix)


tcaption_indices = caption_tokens_to_indices(train_captions, vocabulary, vocabulary[PAD])
tcaption_matrix = \
    batch_captions_to_matrix(tcaption_indices.reshape(-1), vocabulary[PAD], max_len=53).reshape(118287, 5, 53)

vcaption_indices = caption_tokens_to_indices(validation_captions, vocabulary, vocabulary[PAD])
vcaption_matrix = \
    batch_captions_to_matrix(vcaption_indices.reshape(-1), vocabulary[PAD], max_len=53).reshape(5000, 5, 53)

with open('tcapts_matrix.pkl', 'wb') as f:
    pickle.dump(tcaption_matrix, f)

with open('vcapts_matrix.pkl', 'wb') as f:
    pickle.dump(vcaption_matrix, f)
